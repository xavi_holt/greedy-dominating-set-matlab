function [ S ] = partial_dominating_set ( G, p)
%Given G and percent coverage reqt. p, returns the partial dominating set.
%S is our set of dominating elements.
S = [];

% U are the edges covered by our S.
U = zeros(1,length(G));

%cov is |U|
cov = 0;

%t represents candidate edges for adding to S
t = ones(1,length(G));

while cov < p*length(G)
    
    max = 0;
    v = 0;
    
    for i = 1:length(G)
        
        if t(i) == 1
            
            if nnz(U + G(i,:)) > max
                max = nnz(U + G(i,:));
                v = i;
            end
            
        end
        
    end
    
    U(v) = 1;
    U = U + G(v,:);
    
    rm = [find(G(v,:)) v];
    t(rm) = 0;
    
    cov = max + 1;
    S = [S v];
    
end

end



