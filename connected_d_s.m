function [ S ] = connected_d_s(G, v)
%Given G and starting vertex v, returns the connected dominating set.

%S is our set of dominating elements.
S = [];

% U are the edges covered by our S.
U = zeros(1,length(G));

%cov is |U|
cov = 0;

%t represents candidate edges for adding to S
t = zeros(1,length(G));
t(v) = 1;

while cov < length(G)
    
    max = 0;
    v = 0;
    
    for i = 1:length(G)
        
        if t(i) == 1
            
            if nnz(U + G(i,:)) > max
                max = nnz(U + G(i,:));
                v = i;
            end
            
        end
        
    end
    
    U(v) = 1;
    U = U + G(v,:);
    
    rm = G(v,:);
    rm(v) = 1;
    
    t = t + rm;
 
    cov = max;
    S = [S v];
    
end

end



